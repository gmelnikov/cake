package main

import (
	"flag"
	"fmt"
	"math/rand"
	"time"

	"rsc.io/quote"
)

func main() {
	q := flag.Bool("q", false, "say something after cake")
	flag.Parse()
	fmt.Print("🍰")
	if *q {
		quotes := []func() string{
			quote.Go,
			quote.Hello,
			quote.Glass,
			quote.Opt,
		}
		rand.Seed(time.Now().UnixNano())
		fmt.Print(" " + quotes[rand.Intn(len(quotes))]())
	}
	fmt.Println()
}
